---
layout: page
title: 我喜欢的歌曲
---

托管在 Dropbox ... via [X.cat](https://wj.qq.com/s/1611888/76c0/)

---

飘雪 - [🔗](https://music.163.com/m/song?id=212279)

<audio src="https://www.dropbox.com/s/27msjlkeudxeotm/%E9%A3%98%E9%9B%AA%20-%20%E9%99%88%E6%85%A7%E5%A8%B4.mp3?dl=1" controls></audio>

家乡 - [🔗](https://www.xiami.com/song/1770490863)

<audio src="https://www.dropbox.com/s/izsrbqjf8jdejd2/%E5%AE%B6%E4%B9%A1Finale.mp3?dl=1" controls></audio>

无果 - [🔗](https://www.xiami.com/song/1771385076)

<audio src="https://www.dropbox.com/s/zi4ogal9ppt5kb9/%E6%97%A0%E6%9E%9C.mp3?dl=1" controls></audio>

一笑中 - [🔗](https://www.xiami.com/song/1771138658)

<audio src="https://www.dropbox.com/s/rj99om571b5qbfv/%E4%B8%80%E7%AC%91%E4%B8%AD.mp3?dl=1" controls></audio>

友谊之光 - [🔗](https://music.163.com/m/song?id=5258624)

<audio src="https://www.dropbox.com/s/icilqfm0c7tzbsk/%E5%8F%8B%E8%B0%8A%E4%B9%8B%E5%85%89.mp3?dl=1" controls></audio>

Fetish - [🔗](https://www.xiami.com/song/1796413433)

<audio src="https://www.dropbox.com/s/bz6cp0p2piun8lu/Fetish.mp3?dl=1" controls></audio>

Lydia - [🔗](https://www.xiami.com/song/1769989678)

<audio src="https://www.dropbox.com/s/a8mjndlfnjejpw1/Lydia.mp3?dl=1" controls></audio>

Try - [🔗](Try》https://www.xiami.com/song/1771306882)

<audio src="https://www.dropbox.com/s/d301olh5fao07ty/Try-Pnk.mp3?dl=1" controls></audio>

Walk away - [🔗](https://www.xiami.com/song/1795775760)

<audio src="https://www.dropbox.com/s/zbbutf2hiog2o88/Walk%20Away-LVNDSCAPE-Kaptan.mp3?dl=1" controls></audio>

Yesterday Once More - [🔗](https://www.xiami.com/song/2088864)

<audio src="https://www.dropbox.com/s/vh6hwdj1zbhe6lu/Yesterday%20Once%20More%20-%20Carpenters.mp3?dl=1" controls></audio>

...

<!--

<audio src="" controls></audio>


<audio src="" controls></audio>


<audio src="" controls></audio>

- [🔗]()

-->


